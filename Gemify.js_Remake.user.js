// ==UserScript==
// @name         Gemify.js Remake
// @namespace    http://gitlab.com/carbonwind
// @version      2.0.1
// @description  Gemify remade to work in 2024
// @author       carbonwind
// @require      https://code.jquery.com/jquery-3.6.0.min.js
// @include      /^https?:\/\/steamcommunity\.com\/(?:id|profiles)\/.*\/inventory
// @exclude      /^https?:\/\/steamcommunity\.com\/(?:id|profiles)\/.*\/inventoryhistory
// @icon         data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAIMklEQVRYw+1Wa3BdVRX+1t7nnHvPfeY+kpvk5ialbZpMS0MttmAVLWWAEbAzpRMVHEB8PwZ0HGQ6ijPoKAwMjoLCDwdpmVpaLBWLAjKV8ioyUGgRS18hachN0qTJfT/PPefs5Y+k5dVK6R//8M3sH3vP3mt961t777UI/wNvXfPl98znb9yCH03VIIjgMqNhObBqLsAABAEM3N8dxkcBnWzxG7vHwAyYbh2/W9sDjDt45I678eTKtWiaG4PLCoIEGpZDVt1lqDMnIE62uHXzbty/rB2HRgqorv4ifvXnHei/+UYAgNtwIEmAACHEzHGSM87PBCdV4OrnjkCAyFZuXApUol6tVpBe1l0bk3tHsegrnwQJonrZQiHfYE28Y+b+7jCu3NYJfpfxv6wdOT0F0v2rke5fjaapCdgNB47tBmxH6b8/r4t11yYA1HJOknJDUzIzOOWvZqokahaYGWxoYE1+IEAGsGZb50dLwU+23Ib45Nu8ZPCN4TUvP1G88dkD71aMla00tlXQrdsClk0o1wQyRSBXAnAiG3ScwGmnIN2/eoY1Q1ZKJaf3qWcw3v8FYoAveegxnL9n7J1zzJIVuwAgDE1CkAuA2XaRG13xAUePniIN71HAEARBhJBHGh3NEbnz0s9CEGmSSPzjuv6ZTT4PYOqgsA8y4oeMBnQKmF7oOlPQd7IA6fRfAQHMzAAEA7Q0HochyG372XZ14dy1eObxXRg9MAQi4SWiC0EUISAAcCsrRfaeQYD0M3+GjsvY/OokiLgKAdck1mO961Xrxj95XKI42O0cG0yHhK4lAeQBVOCqHBQfgdVgOacFdm3X8ZDfE/mabZ0nxikJKDBuuvwcNFzR4tjc9GNvt5uc3LhcU849TNp28vieaG+O/8bOFS8FoWo2+W326B54DZe8BrhqoZJZd/wSytlx+goQEVC3QIDrUa7a5E0aTHwjMX/LMDwrwuHworbmyPV6pnDV3uf/E8kXyn4SFBOZAmC7kHNa4IldeTx48S4Cp7wLJwhkr14Lj9SQqRag2PVXiD7/XPbFMBONqYYFjxRoCvkxZSuqKHy6Jxq+b/il4Yv2HsxOk6EnAAQ5GUCg8xeInT8MAM6s0+isH/pQBQQReTSPEERNRNS3wCnpjjReZyHryrWhDB0FQRiu2WgY2uJk1H+vnqvcULHV5zRDtlG6GGW3bqjCwePfgQvAmrHO4pQE1qdSeOH1fYg9tI11u+4vDw3tnz46eefEv9/Mr0q/uS9ilffUyiWoUgVtXg/isRCqAS8cv2ifF9Z+fnQwf/me/Tk31BwoSelzfMEgXFsdt18H0AfQVQCCH3inD3fNQZ0VCuWS0d7ZdX7bgu75qlxpIcdu9Ztmm1atRd72R3v/2LYkNTBnES5esRDRRAhZITFZtcGZEiIW7KNlfmykJm7p73vxYEQewrfPvg1XPNzWTSxyUpPtRFgO4BEA+VDYD0e52HTJYdDWOWehz2ym1yoTX9IN/a7E/O4WfzymNTJZIqXADQuaIEwILzZ0nYfpC1bhinPno7M5hEEFpAs18HQJfkfCUpNPN0Xu2e6HXm2YxUjZzh4t2vn9pUphZGBoqPzAtRusu3bcgapbw+K2T+DQsTdBGzs74TL3+aXY7DOMhaHWBFSlCmE3EAr64Q/4YAZ8MAN+vBJOYV10KcyeXqycl0As6sWEqSOmFFJVByNTaRxTm9zzUj7lM6rSpYnqtF06mqvzwWqZd9s27xkrHnnD4vpRQdIhEOiBjg7UlLohKOVvQ16viMaaEPebiLe3wBMOgxItoGQHpNSQPTiADXoSO/pWoKb7EfZosMMamhMhXGIYyI+XMDQ9gGTLozg3NY1OfwSWm8NgMYeBnMuZeqM8URrb5Nh8q2NUJ003ALop2UoLIL7eAnGXlxB2TS+i8Qgi7W3wpDrhkABZFrR6DVpmCmwGsKtzMR5MLHBHkynREo9S0NQQEkA928D0lIUAvYYL6WEsjS1CNenimG8MA8VxTFXrKNYL46zE93VNf861VZ7WdyR9BFzcBLqli6jHIDLzYMcmamhSKmL2BqTm1aVQDcBqSJGpwjvw0qqzrCNrlq4cFb1eW/VAlm2YVRt5S0dg5DB+OvQ3JEIBPO87lPvXqmwOPjumRENrsBoQQt8oSf5T6jhAGzqSJoCzGbjKA6QCRH4makghspqUZDLHgoyUI0WtCmQtwkgjGH/r0NfmLkpd1nQtabq2aWhJ7bC7TGo128DQpPrU/lfVusaAZk1NVvbq6fU7b649I005XzdEkEgMsUsHwBgWGk3Tho52mi1hHQwkFMHUSLBHCNeQwtRIaILgVQwDSnG6tcv70trvXBNYPG9Jc6QQ7Yk+hd0ZbeDZ2jXTdsBMBQ4MjH3z3tutZdnxZMGxd+1fqm47fGegCIc9zJDMXAGhRAQLDJc2dCTf9zGxZAaIiIUgoQvJNisol+XC0VG+/oe/7jF7F27t7u2cNzY+ti8otuyQwdholq+DZluR3n2vlC7b+gfyF/MgRhqEFyoJyhz4qmanL9UgbLCUgh1HgQh4P4ETlUMIAU0QDCGgGHCVws7e5djat9ITDQYu8AaCHZVa/WWVWHakOe4jr170NmUmzeUvPE5LXtmp/MW8Q8wWGJZw4AbHlPv37SbnesWHd8WnQmrdfXAaDTQlWtE+fy7G02ms/MGVeP3JMnTnbVT9Qey+qAu//O6tWPHkgzMVgGim0AIMBi4aHjpzAgDQ/L3bZxtPhjccxlmfWQlmhpIS3ftexoqn/4rmY+MI5TMgpWb3Emi2UX8/gY/xMf7v+C94p6v/yB+B/AAAAABJRU5ErkJggg==
// ==/UserScript==

// Globals from Steam and jQuery are used in the script
/* globals $ g_ActiveInventory g_sessionID g_steamID g_strProfileURL g_bViewingOwnProfile ReadInventoryHash UserYou */

(function() {
    'use strict';
    // Logs Steam global variables for session and ID64 in the console
    console.log("Debug - g_sessionID: " + g_sessionID);
    console.log("Debug - g_steamID: " + g_steamID);

    // Global variables
    var selected_items = [];
    var gems_obtained = 0,
        items_total = 0,
        items_grinded = 0,
        items_processed = 0,
        interval;

    class Item {
        constructor(p_appid, p_assetid, p_contextid) {
            this.appid = p_appid;
            this.assetid = p_assetid;
            this.contextid = p_contextid;
        }
    }

    function addElements() {
        var insert_region = document.getElementsByClassName("filter_ctn inventory_filters")[0];

        // Make button and specify styles
        var btn = document.createElement('a');
        btn.id = "gemify-btn";
        btn.className = "btn_small btn_green_white_innerfade";

        // Button text
        var btn_txt = document.createElement('span');
        btn_txt.id = "gemify-btn-txt";
        btn_txt.innerText = 'Start Gemify';
        btn.appendChild(btn_txt);

        // Insert button
        insert_region.appendChild(btn);
        btn.addEventListener("click", start);

        // Gemify status
        var gemify_status = document.createElement('span');
        gemify_status.id = "gemify-status";

        $(gemify_status).css({
            "margin-left": "10px",
            "color": "gold",
            "font-size": "12px",
            "font-family": "'Motiva Sans'"
        });

        gemify_status.innerText = "Gemify: Awaiting startup";
        insert_region.appendChild(gemify_status);
    }

    function selectItem(p_contextid, p_assetid, p_gemifyid) {
        var item = document.getElementById(p_gemifyid);

        for (var i = 0; i < selected_items.length; i++) {
            if (selected_items[i].assetid == p_assetid) {
                console.log("Deselected " + p_gemifyid);
                selected_items.splice(i, 1); // Remove from selected items
                item.style.removeProperty("border");

                countSelectedItems();
                return false;
            }
        }

        console.log(`Selected ${p_gemifyid} - asset ${p_assetid}`);

        /* Steam's AJAX for gems no longer accepts Steam's appID, we are
        *  using market_fee_app associated to the selected item as a workaround
        */
        let appid = g_ActiveInventory.selectedItem.description.market_fee_app

        selected_items.push(new Item(appid, p_assetid, p_contextid));
        item.style.border = "3px dashed white";
        countSelectedItems();

        return true;
    }

    function countSelectedItems() {
        if (selected_items.length > 0) {
            document.getElementById('gemify-status').innerText = `${selected_items.length} element${selected_items.length > 1 ? "s":""} selected`;
            document.getElementById('gemify-status').style.color = `white`;
        } else {
            document.getElementById('gemify-status').innerText = `No element selected`;
            document.getElementById('gemify-status').style.color = `orange`;
        }
    }

    function start() {
        document.getElementById("gemify-btn-txt").innerText = "Gemify selected items";
        interval = setInterval(updateSelectionEvents, 500);

        document.getElementById("gemify-btn").removeEventListener("click", start);
        document.getElementById("gemify-btn").addEventListener("click", gemifySelected);

        document.getElementById('gemify-status').style.color = "orange";
        document.getElementById('gemify-status').innerText = "No element selected";
        alert("Gemify has been started. Select the items you want to gemify, then click on 'Gemify selected items'.")
    }

    function updateSelectionEvents() {
        // console.log("Updating selectable items...");

        var item_number = 0
        var items = document.getElementsByClassName("inventory_item_link");
        for (var i = 0; i < items.length; i++) {
            let item_hash = items[i].getAttribute("href");

            if (item_hash.startsWith("#753_6_")) {
                let gemify_id = `gemify_item_${item_number}`;
                let item = ReadInventoryHash(item_hash); // Gets item properties through Steam's function

                items[i].id = gemify_id;
                items[i].onclick = function () {selectItem(item.contextid, item.assetid, gemify_id)};
                item_number++
            }
        }
    }

    function updateGemifyStatus() {
        if (items_grinded === items_total) {
            document.getElementById('gemify-status').style.color = "lime"; // Indicates that all the items are grinded successfully
        } else {
            document.getElementById('gemify-status').style.color = "white";
        }

        if (items_processed === items_total) { // If all the selected items are processed
            document.getElementById("gemify-btn-txt").innerText = "Gemify complete";
            document.getElementById('gemify-status').innerText = `${items_processed} items processed. ${items_grinded}/${items_total} successfully grinded (${gems_obtained} gems). Refresh the page to see changes.`;
        } else {
            document.getElementById('gemify-status').innerText = `${items_processed} items processed. ${items_grinded}/${items_total} successfully grinded (${gems_obtained} gems).`;
        }
    }

    function gemifySelected() {
        if (selected_items.length === 0 ) {
            alert("You have not selected anything!");
            return
        }

        document.getElementById("gemify-btn").removeEventListener("click", gemifySelected);
        document.getElementById("gemify-btn-txt").innerText = "Gemifying...";

        items_total = selected_items.length;
        processSelectedItems();
    }

    function processSelectedItems() {
        if (selected_items.length > 0) {
            let item = selected_items.pop();

            console.log(`Start fetching gem value for asset ${item.assetid}`);

            $.get({
                url: `${g_strProfileURL}/ajaxgetgoovalue/`,
                dataType: "json",
                timeout: 15000,
                data: {
                    sessionid: g_sessionID,
                    appid: item.appid,
                    assetid: item.assetid,
                    contextid: item.contextid
                },
                success: function (response) {
                    if (response.hasOwnProperty("goo_value")) {
                        grindIntoGems(item, response.goo_value)
                    } else {
                        console.log(`Fetch successful, but no value found for asset ${item.assetid}, skipping.`);
                    }
                },
                error: function (jqXHR) {
                    console.log(`Value fetch for asset ${item.assetid} failed: (${jqXHR.status}) ${jqXHR.statusText}, skipping.`);
                },
                complete: function () {
                    setTimeout(processSelectedItems, 500);
                    items_processed++
                    updateGemifyStatus();
                }
            });
        } else {
            console.log(`Finished gemifying all items.`);
        }
    }

    function grindIntoGems(p_item, p_value) {
        console.log(`Start grinding asset ${p_item.assetid} into ${p_value} gems`);

        $.post({
            url: `${g_strProfileURL}/ajaxgrindintogoo/`,
            dataType: "json",
            timeout: 15000,
            data: $.param({
                sessionid: g_sessionID,
                appid: p_item.appid,
                assetid: p_item.assetid,
                contextid: p_item.contextid,
                goo_value_expected: p_value
            }),
            success: function (response) {
                console.log(`Grind asset ${p_item.assetid} into gems success (${p_value} gems)`);
                gems_obtained += parseInt(p_value);
                items_grinded++
            },
            error: function (jqXHR) {
                console.log(`Grinding asset ${p_item.assetid} failed: (${jqXHR.status}) ${jqXHR.statusText}, skipping.`);
            },
            complete: function () {
                updateGemifyStatus();
            }
        })
    }

    if (g_bViewingOwnProfile) {
        addElements();
    }

})();
