# Gemify.js Remake

Remake of https://github.com/SleepyAkubi/gemify.js as a Tampermonkey script that works in 2024

## How to install

**Prerequisites:**
- A modern web browser (i.e Firefox, Chrome, etc). This means no Internet Explorer.
- Tampermonkey (https://www.tampermonkey.net/)

Once you have both prerequisites, simply go here to install the userscript: 
https://gitlab.com/carbonwind/gemify.js-remake/-/raw/main/Gemify.js_Remake.user.js?inline=false

A prompt asking you to install the script should show up.
